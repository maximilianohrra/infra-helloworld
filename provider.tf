provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.aws_region
}

resource "aws_instance" "helloworld" {
  ami           = var.aws_ami
  instance_type = var.aws_instance_type

  vpc_security_group_ids = [aws_security_group.helloworld.id]
  key_name               = var.aws_key_name

  tags = {
    Name = "helloworld-instance"
  }
}

resource "aws_security_group" "helloworld" {
  name        = "example"
  description = "Example security group"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

variable "aws_region" {
  default = ""
}

variable "aws_ami" {
  default = ""
}

variable "aws_instance_type" {
  default = ""
}

variable "aws_key_name" {
  default = ""
}
